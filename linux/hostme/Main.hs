{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Control.Exception          (try)
import qualified Data.ByteString.Lazy.Char8 as L8
import           Network.HTTP.Simple
import Data.ByteString.Lazy as L
import Data.ByteString.Builder as B

main :: IO ()
main = do
    eresponse <- try $ httpLBS "http://does-not-exist"

    case eresponse of
            Left e -> print (e :: HttpException)
            Right response -> Prelude.putStrLn "Not The Flag"

